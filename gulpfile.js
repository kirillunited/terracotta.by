var gulp = require("gulp");
var pagebuilder = require('gulp-file-include');

gulp.task('inject', function () {
    var htmlInject = gulp.src('src/html/index.html')
        .pipe(pagebuilder({
            prefix: '@@',
            basepath: '@file'
          }))
        .pipe(gulp.dest('build/'));
});

gulp.task('watch', ['inject'], function () {
    gulp.watch('src/**/*', ['inject']);
});

gulp.task('default', ['watch']);