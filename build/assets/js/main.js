// BEGIN: card
$(document).ready(function () {
    $('body').on('click', '.description_open', function () {
        $(this)
            .hide()
            .siblings('.description_text')
            .addClass('description_text-shown');
        return false;
    });
    $('body').on('click', '.slider-nav .slider_item', function () { // edit 18-07-2018
        var el = $(this);
        el.siblings().removeClass('slider_item-active');
        el.addClass('slider_item-active');

        var currentSlide = $(this).attr('data-slick-index');
        $('.slider-for').slick('slickGoTo', currentSlide);
    });
    /** BEGIN: edit 18-07-2018 */
    $('body').on('click', '.card .footer_slider .slider_item:not(.slick-center)', function () {
        return false;
    });
});

/** END: edit 18-07-2018 */

// END: card