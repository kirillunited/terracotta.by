var minPrice = 99999999;
var maxPrice = 0;
var filterPriceFrom = 0;
var filterPriceTo = 0;

var JSTerraDD = function(){
	
	this.zone = $("#dnd-zone");
	this.input = $("#dnd-file");
	this.dd_files = null;
	this.dd_sendFiles = [];
	this.input_files = null;
	this.input_sendFiles = [];
	this.limit = 10485760;
	var __this = this;

	this.render = function(){
		if(this.zone.length){
			this.zone
				.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
					e.preventDefault();
					e.stopPropagation();
				 })
				.on('dragover dragenter', function() {
					__this.zone.addClass('is-dragover');
				})
				.on('dragleave dragend drop', function() {
					__this.zone.removeClass('is-dragover');
				})
				.on('drop', function(e) {
					__this.drop(e);
				})
		}	
		if(this.input.length){
			this.input.on("change", function(){

				__this.removeInputFiles();
				__this.sizeErrorHide();
				if(this.files.length){
					__this.input_files = this.files;
					__this.error = false;
					__this.validate();
				}

			})
		}
		$("body").on("click", ".calculation__delete", this.deleteFile);
	}
	this.deleteFile = function(){
		var index = $(this).data("index");
		var parent = $(this).parent(".calculation__uploaded");
		var dd = parent.hasClass("upload__dnd_file");
		var i = false;
		if(dd){
			i = __this.arraySearch(index, __this.dd_sendFiles)
			if(i !== false){
				__this.dd_sendFiles.splice(i, 1);
			}
		}
		else{
			i = __this.arraySearch(index, __this.input_sendFiles)
			if(i !== false){
				__this.input_sendFiles.splice(i, 1);
			}
		}
		parent.remove();

	}
	this.arraySearch = function(val, arr){
		for(var i = 0; i < arr.length; i++){
			if(arr[i] == val){
				return i;
			}
		}	
		return false;
	}
	this.drop = function(e){
		this.removeDnD();
		this.sizeErrorHide();
		if(!e.originalEvent.dataTransfer.files.length){
			return false;
		}
		this.dd_files = e.originalEvent.dataTransfer.files;
		this.validate(true);
	}
	
	this.sizeValidate = function(){
		var size = this.getSizeDD() + this.getSizeInput();	
		return this.limit >= size;
	}
	this.getSizeInput = function(){
		if(this.input_files == null){
			return 0;
		}
		var size = 0;
		for(var i = 0; i < this.input_files.length; i++){
			size += this.input_files[i].size;
		}
		return size;
	}
	this.getSizeDD = function(){
		if(this.dd_files == null){
			return 0;
		}
		var size = 0;
		for (var i = 0; i < this.dd_files.length; i++) {
			size += this.dd_files[i].size;
			
		}
		return size;
	}
	this.removeDnD = function(){
		this.dd_files = null;
		this.dd_sendFiles = [];
		$(".upload__dnd_file").remove();
	}
	this.removeInputFiles = function(){
		this.input_files = null;
		this.input_sendFiles = [];
		$(".upload__input_file").remove();


	}
	this.sizeError = function(){
		this.error = true;
		$('.calculation__upload').append(
			'<div class="calculation__uploaded calculation__uploaded--error size">\
				<span class="calculation__error">Ошибка.Файлы не загружены. Размер файлов превышает 10 Мб</span>\
			</div>');
	}
	this.sizeErrorHide = function(){
		$(".calculation__uploaded--error.size").remove();
	}
	this.validate = function(dd){
		if(!this.sizeValidate()){
			this.sizeError();
			return false;
		}
		
		this.showFiles(dd);
	}
	this.showFiles = function(dd){
		var files = dd ? this.dd_files : this.input_files;
		var indexes = [];

       var formFixed = $(".calculation__field--fixed");

		if(files.length >= 1) {
            formFixed.addClass("fixed");
		} else {
			formFixed.removeClass("fixed");
		}

		for (var i = 0; i < files.length; i++) {
			var file = files[i];
			if(!this.isType(file.name)){	
				this.showFileError(file.name, dd);
				continue;
			}
			indexes.push(i);
			this.showFile(file.name, dd, i);
		}
		if(dd){
			this.dd_sendFiles = indexes;
		}
		else{
			this.input_sendFiles = indexes;
		}
		
	}
	this.showFile = function(name, dd, index){
		var className = dd ? "upload__dnd_file": "upload__input_file";
		$('.calculation__upload').append('<div class="calculation__uploaded '+ className +'">\
                            <span>' + name + '</span>\
                               <a href="javascript:void(0);" data-index="'+index+'"  class="calculation__delete">\
                                <img src="/local/templates/terra/img/delete-download.png" alt="">\
                                </a>\
                        	</div>');

	}
	this.showFileError = function(name, dd){
		var className = dd ? "upload__dnd_file": "upload__input_file";
		$('.calculation__upload').append('<div class="calculation__uploaded calculation__uploaded--error '+ className +'">\
                            <span>' + name + '</span>\
                            <span class="calculation__error">Ошибка.Файл не загружен. Неверный формат файла, возможные форматы jpeg, png, pdf</span>\
                        	</div>');

	}
	this.getType = function(file){
		return file.split(".")[file.split(".").length - 1].toLowerCase();
	}
	this.isType = function(file){
		var type = this.getType(file);
		switch (type) {
			case 'png':
			case 'jpeg':
			case 'jpg':
			case 'pdf': return true;
		};
		return false;
	}
}

$(window).resize(function() {
    sliderAdvertisingStart()
});

function sliderAdvertisingStart() {
    var $soc_a = $('.advantages-box');
    if($(window).width() < 767) {
        $soc_a.not('.slick-initialized').slick({
            infinite: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: false,
        });

    } else{
        if($soc_a.hasClass('slick-initialized')) {
            $soc_a.slick("unslick");
        }
    }
}


/*Calculation*/
    (function(){
        //Удаление загруженных файлов
	/*	$(".calculation__delete").on("click", function(e){
             e.preventDefault();
            $(this).parent(".calculation__uploaded").hide(500);
         });
		*/
		$(".calculation__popup-link").on("click", function(e){
			e.preventDefault();
			$(".overlay").hide();
			$(".calculation__popup").hide();
		});
}());


function showError(arFields){
	if(arFields.fatal){
		alert(arFields.fatal);
		return false;
	}
	var type_field = "";
	var matchs = "";
	var coutnError = 0;
	for(key in arFields){
		
		var item = "";
		if(arFields[key].id){
			item = $("#"+arFields[key].id);
		}
		else{
			item = $(".calculation__form input[name='"+arFields[key].name+"']")
		}
		type_field = item.data("type");
		console.log(type_field);
		switch(type_field){
			case "text":
				matchs = String(arFields[key].value).search(/^.+$/);
			break;
			case "phone":
				matchs = arFields[key].value.replace(/\s+/g,'').search(/^[\+]\d+$/);
			break;
			case "email":
				matchs = arFields[key].value.search(/^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/);
			break;
			case "file":
				matchs = arFields[key].value ? 1 : -1;
			break;
		}
		if(!arFields[key].value){
			
			$(".calculation__form input[name='"+arFields[key].name+"']").addClass("error-border");
			$(".calculation__form textarea[name='"+arFields[key].name+"']").addClass("error-border");
			if(type_field == "file"){
				
				item.parent("label").addClass("error-border");
			}
			++coutnError;
		} else {
			if(matchs <= -1){
				$(".calculation__form input[name='"+arFields[key].name+"']").addClass("error-border");
				$(".calculation__form textarea[name='"+arFields[key].name+"']").addClass("error-border");
				if(type_field == "file"){
					$(".calculation__form input[name='"+arFields[key].name+"']").parent().find('label').addClass("error-border");
				}
				++coutnError;
			}
			else {
				$(".calculation__form input[name='"+arFields[key].name+"']").removeClass("error-border");
				$(".calculation__form textarea[name='"+arFields[key].name+"']").removeClass("error-border");
				if(type_field == "file"){
					item.parent("label").removeClass("error-border");
				}
			}
		}
	}
	return coutnError;
};

$(document).ready(function () {
	window.TerraDnD = new JSTerraDD();
	window.TerraDnD.render();
	
	$('body').on('submit', '.calculation__form', function(){
		var arFields = $(this).serializeArray();
		var isFiles = (!!window.TerraDnD.dd_sendFiles.length || !!window.TerraDnD.input_sendFiles.length);
		//arFields[arFields.length] = {name:"file_calc", value:isFiles, id:"dnd-file"};
		if(showError(arFields) <= 0){
			var formData = new FormData(this);
			var counter = 0;
			if(window.TerraDnD.dd_sendFiles.length){
				for(var i = 0; i < window.TerraDnD.dd_sendFiles.length; i++){
					formData.append("order_files[]", window.TerraDnD.dd_files[i]);
				}
			}
			if(window.TerraDnD.input_sendFiles.length){
				for(var i = 0; i < window.TerraDnD.input_sendFiles.length; i++){
					formData.append("order_files[]", window.TerraDnD.input_files[i]);
				}
			}
			
			formData.append("ajax_popap", "Y");
			$.ajax({
				url: location.href,
				type: 'POST',
				data: formData,
				cache: false,
				dataType: 'json',
				processData: false, // Не обрабатываем файлы (Don't process the files)
				contentType: false, // Так jQuery скажет серверу что это строковой запрос
				success: function( respond, textStatus, jqXHR ){
					console.log("ok");
					console.log(arFields);
					var arFields = respond;
					if(arFields.OK == "Y"){
					  if($('.calculation__form').hasClass('calculation__form_for_page')==false) {
                          $('.calculation__form-close').click();
                          $(".overlay").show();
                          $(".calculation__popup").show();
                          $('.calculation__form')[0].reset();
                          window.TerraDnD.removeInputFiles();
                          window.TerraDnD.removeDnD();
                      }else{
					  	$('.calculation__form_for_page .calculation__field').hide();
					  	$('.calculation_form_result').show();

					  }

						$('.calculation__fake-text').html('<span>Выберите с диска</span> <span>или перетащите</span>');

					} else {
						//console.log(arFields);
						showError(arFields);
					}
				},
				error: function( jqXHR, textStatus, errorThrown ){
					console.log('ОШИБКИ AJAX запроса: ' + textStatus );
				}
			});
		}
		return false;
});

    // new corusel js
	$(".video-slider__preview").fancybox({
            padding: 0,
            openEffect: 'fade',
            closeEffect: 'fade',
            type: 'iframe',
            openEffect: 'none',
            closeEffect: 'none',
            helpers: {
                media: {},
                overlay: {
                    locked: false
                }
            },
        }
   	);
	
	$('.video-slider').slick({
       		dots:false,
   		arrows:true,
   		slidesToShow: 5,
   		slidesToScroll: 1,
      	infinite:true,
   		centerMode: true,
  		centerPadding: '10px',
  		variableWidth: true
    });
	

    $('.corusel-main').slick({
        dots:true,
        arrows: true,
        adaptiveHeight: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        speed: 1200,
        autoplaySpeed: 3700,
        pauseOnHover: true,
        pauseOnDotsHover: false
    });
    $('.corusel-main__item').css({'height' : $('.corusel-main').height()});
    $(window).resize(function () {
        $('.corusel-main__item').css({'height' : 'auto'});
        $('.corusel-main__item').css({'height' : $('.corusel-main').height()});
    });




    $('.corusel-main .slick-dots li').each(function(){
        $(this).append('<div class="pie spinner"></div><div class="pie filler"></div><div class="mask"></div>');
    })
    $('.corusel-main .slick-dots li button').each(function(){
        var pagerNamber = $(this).parent().index();
        pagerNamber = '0' + ++pagerNamber
        $(this).text(pagerNamber)
    });

    // new js
    sliderAdvertisingStart();


    $('.main-page__slider').slick({
        dots:false,
        fade: true,
        arrows: true,
        adaptiveHeight: false,
        infinite: true,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 3000
    });

    /*
     $('.catalog-list-sl').slick({
     dots: false,
     infinite: true,
     speed: 300,
     slidesToShow: 1,
     centerMode: false,
     variableWidth: true
     }); */

    $('.catalog-list-sl').owlCarousel({
        margin:0,
        autoWidth:true,
        autoHeight:true,
        nav:true,
        loop:true,
		responsive : {
			0 : {
				items:1,
			},
			480 : {
				items:2,
			},
		},
	 });



$('.project-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true
    });
    // end new js


    // detect touch device
    if (isTouchDevice() === true) {
        $('body').addClass('touch');
    } else {
        $('body').addClass('no-touch');
    }

    if($('.filter_btn').length) {
        var filter = '<div class="filter_btn filter_btn-footer">' + $('.filter_btn').html() + '</div>';
        $("body").append(filter);
    };
    $('.filter_open').click(function(){
        $('.filter_bg').show();
		$('body').addClass('bfixed');
        $('body, html').animate({scrollTop: 0}, 1000);
        $('.filter, .filter_btn-footer').animate({'right': 0}, 500);
        return false;
    });
    $('.filter_mob_close, .filter_bg, .filter_btn .btn_red').click(function(){
        $('.filter_bg').hide();
        $('').fadeOut();
		$('body').removeClass('bfixed');
        $('.filter, .filter_btn-footer').animate({'right': -320 + 'px'}, 500);
        return false;
    });

    $('.clear_filter').click(function(){

        $("input[name='color-item']").removeAttr("checked");
        $(".clear-parameter").click();
        console.log(8787878);
        return false;
    });



    //NEWS-SLIDER
    if ($('.news-in__slider').length > 0) {
        var $status = $('.pagingInfo');
        var $slickElement = $('.news-in__slider');
        $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
            var i = (currentSlide ? currentSlide : 0) + 1;
            $status.text(i + '/' + slick.slideCount);
        });

        $slickElement.slick({
            dots: false,
            fade: true,
            arrows: true,
            adaptiveHeight: true,
            prevArrow: $('.prev'),
            nextArrow: $('.next'),
            responsive: [
                {
                    breakpoint: 551,
                    settings: {
                        dots: true,
                        fade: true,
                        adaptiveHeight: true,
                        arrows: false
                    }
                },
            ]
        });

        //PRELOADER
        var opts = {
            lines: 11
            , length: 19
            , width: 2
            , radius: 11
            , scale: 0.5
            , corners: 0.9
            , color: '#000'
            , opacity: 0.25
            , rotate: 0
            , direction: 1
            , speed: 1
            , trail: 60
            , fps: 20
            , zIndex: 2e9
            , className: 'spinner'
            , top: '50%'
            , left: '50%'
            , shadow: false
            , hwaccel: false
            , position: 'absolute'
        };
        var target = document.getElementById('news-loader');
        var spinner = new Spinner(opts).spin(target);

        $('#news-loader').fadeOut(500, function () {
            $('.news-in__slider').animate({'opacity': '1'}, 1000);
        });
    }
    ;


    //TABS-CATALOG
/*    $(".catalog-tab_content").hide();
    if (location.pathname == '/catalog/') {
        $("ul.catalog-tabs:not(.cancel) li:first").addClass("active").show();
        $(".catalog-tab_content:first").show();
    } else {
        path = location.pathname.split('/');
        fpath = "/" + path[1] + "/" + path[2] + "/";

        $("ul.catalog-tabs:not(.cancel) a[href='" + fpath + "']").parents('li').addClass("active").show();
        activeTab = $("ul.catalog-tabs:not(.cancel) a[href='" + fpath + "']").attr('data-id');
        $(activeTab).fadeIn();

        if ($('.filter-submenu__item.active a').length) {
            $(activeTab).attr('data-section-id', $('.filter-submenu__item.active a').attr('data-section-id'));
        }
    }
    */


    //MOBILE MENU
    var mySlidebars = new $.slidebars({
        disableOver: 960
    });
    $('#nav-toggle').on('click', function () {
        /*mySlidebars.slidebars.toggle('left');*/
        $(".sb-slidebar").toggleClass("active");
    });

    //MOBILE-PHONE
    $('#mobile-phone').on('click', function () {
        $(this).toggleClass('active');
        $('.mobile-phone__menu').fadeToggle(0);
        return false
    });


    //CATALOG-FILTER SUBMENU
    $('.filter-list__item > a').on('click', function () {

        console.log(this);

        $(this).parents('.filter-list__item').siblings('.filter-list__item').find('a').removeClass('visible');
        $(this).toggleClass('visible');

        $(this).parents('.filter-list__item').siblings('.filter-list__item').find('.filter-submenu').fadeOut();
        $(this).next('.filter-submenu').fadeToggle();
        return false
    });

    setMinMaxPrice();

    filterPriceFrom = minPrice;
    filterPriceTo = maxPrice;

    $('.filter-submenu a').click(function (event) {
        if ($(event.target).closest(".filter-list__item > a, .filter-check").length) return;
        $('.filter-submenu').fadeOut();
        $(".filter-list__item > a").removeClass('visible');

        $(event.target).parents('li').find('a:first').html($(event.target).html());

        if ($(event.target).attr('data-fabric-id')) {
            createCookie('data-fabric-id', $(event.target).attr('data-fabric-id') + '=' + $('.catalog-tabs .active a').attr('data-id'), 3);
            $(event.target).parents('.catalog-tab_content').attr('data-fabric-id', $(event.target).attr('data-fabric-id'));
        }

        //if ($(event.target).attr('data-section-id')) {
        //    createCookie('data-section-id', $(event.target).attr('data-section-id') + '=' + $('.catalog-tabs .active a').attr('data-id'), 3);
        //    $(event.target).parents('.catalog-tab_content').attr('data-section-id', $(event.target).attr('data-section-id'));
        //}
        //showProducts(false, true);

        //event.stopPropagation();
    });

    var inProgress = false;

    /* fix for ipad */
    document.addEventListener("touchmove", ScrollStart, false);

    function ScrollStart() {
        var windowHeight = $(window).height(),
            footerH = $('.page-footer').height(),
            blockItem = $('.catalog-tab_container .catalog-list__item').height() ;

        inProgress = true;

        if(windowHeight - ($('.page-footer').position().top + footerH) > 0){
            showProducts(true);
        }
        inProgress = false;
    }
 /* end fix */
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() + $('.page-footer').height() >= $(document).height() && !inProgress) {
            inProgress = true;
            showProducts(true);
            inProgress = false;
        }
    });


    //NEWS-SLIDER
    if ($('.about-slider').length > 0) {
        $('.about-slider').slick({
            dots: false,
            fade: true,
            arrows: true,
            adaptiveHeight: true,
            infinite: true,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                        dots: true
                    }
                },
            ]
        });

        //PRELOADER
        var opts = {
            lines: 11
            , length: 19
            , width: 2
            , radius: 11
            , scale: 0.5
            , corners: 0.9
            , color: '#000'
            , opacity: 0.25
            , rotate: 0
            , direction: 1
            , speed: 1
            , trail: 60
            , fps: 20
            , zIndex: 2e9
            , className: 'spinner'
            , top: '50%'
            , left: '50%'
            , shadow: false
            , hwaccel: false
            , position: 'absolute'
        };
        var target = document.getElementById('about-loader');
        var spinner = new Spinner(opts).spin(target);

        $('#about-loader').fadeOut(500, function () {
            $('.about-slider').animate({'opacity': '1'}, 1000);
        });
    }


    //INDEX SLIDER
    var indexSlider = $('.index__slider__inner');
    var widthBody = $(window).width();
    var indexSlide = $('.index__slider__slide');
    var indexSidebar = $('.index__sidebar');

    var sliderfull = function () {
        indexSlide.css({'background-position': '50%', 'cursor': 'default'});
        indexSidebar.css({'right': -(indexSidebar.width() + 200)}, 1500);
        $('.index__slider__close').css({'opacity': 1, 'display': 'block'}, 2500);
        $('.index__slider__inner').css({'left': 0}, 2500);
    };
    var sliderfull_close = function () {
        indexSlide.css({
            'background-position': '0 0',
            'cursor': 'url(/local/templates/terra/img/content/zoom-cursor.cur), url(/local/templates/terra/img/content/zoom-cursor.cur), url(/local/templates/terra/img/content/zoom-cursor.png), auto'
        });
        indexSidebar.css({'right': 0}, 1500);
        $(this).css({'opacity': 0, 'display': 'none'}, 2500);
        $('.index__slider__inner').css({'left': -25 + '%'}, 2500);
        // indexSlider.slick('slickPlay');
    };

    indexSlider.slick({
        adaptiveHeight: false,
        // arrows: main_arrows,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 6000,
        infinite: true,
        pauseOnHover: false,
        slidesToShow: 1,
        dots: true
    }).on('click', function (e, slick, currentSlide) {
        indexSlider.slick('slickPause');
    });

    if (widthBody > 960) {
        indexSlide.bind('click', sliderfull);
        $('.index__slider__close').bind('click', sliderfull_close);
    }
    else {
        indexSlide.unbind('click');
        $('.index__slider__close').unbind('click');
    }

    //MAP
    if ( ($('#map_salon').length > 0) && ($('#map_sklad').length > 0) ) {
        ymaps.ready(init);
    }    

    if ($('.news-list').length > 0) {
        $('.news-list').masonry({itemSelector: '.news-list__item'});
    }

    // $('.mobile-menu-submenu-link').on('click', function () {
    //     $(this).toggleClass('active');
    //     $(this).next('.mobile-menu__submenu').fadeToggle();
    //     return false
    // });
        $('.mobile-menu__submenu').show();

    if( $('.catalog-tabs:not(.cancel) li.active a').length > 0){

        var tabId = $('.catalog-tabs:not(.cancel) li.active a').attr('data-id').replace('#catalog-tab', '');

        minPrice = Math.floor(minPrice * 100) / 100;
        maxPrice = Math.ceil(maxPrice * 100) / 100;
        $("input[name='price_from']").val(minPrice);
        $("input[name='price_to']").val(maxPrice);

        $('.change-price').on('keyup', function () {
            filterPriceFrom = $("input[name='price_from']").val();
            filterPriceTo = $("input[name='price_to']").val();
            showProducts(false, true, true);
        });

        /*
        if ($("#slider-range" + tabId).length > 0) {

            $("#slider-range" + tabId).slider({
                range: true,
                min: minPrice,
                max: maxPrice,
                values: [minPrice, maxPrice],
                step: 0.1,
                slide: function (event, ui) {
                    $("#amount" + tabId).val(BX.util.number_format(ui.values[0], 2, '.', ' ') + " до " + BX.util.number_format(ui.values[1], 2, '.', ' '));

                    filterPriceFrom = ui.values[0];
                    filterPriceTo = ui.values[1];
                    showProducts(false, true, true);
                }
            });
            $("#amount" + tabId).val(BX.util.number_format($("#slider-range" + tabId).slider("values", 0), 2, '.', ' ') +
                " до " + BX.util.number_format($("#slider-range" + tabId).slider("values", 1), 2, '.', ' '));
        }
        */
    }


    //if (readCookie('catalog-tab')) {
    //    catalogTab = $(readCookie('catalog-tab'));
    //    $('.catalog-tabs a[data-id="' + readCookie('catalog-tab') + '"]').click();
    //} else {
    //    catalogTab = $('.catalog-tab_content:first');
    //}

    if (readCookie('data-fabric-id')) {
        var cookie = readCookie('data-fabric-id').split('=');
        $(cookie[1] + ' .filter-submenu__item a[data-fabric-id="' + cookie[0] + '"]').click();
    }else{
        showProducts(false, true);
    }

    //if (readCookie('data-section-id')) {
    //    var cookie = readCookie('data-section-id').split('=');
    //    $(cookie[1] + ' .filter-submenu__item a[data-section-id="' + cookie[0] + '"]').click();
    //}

    // functions
    function init() {
        var map_salon = new ymaps.Map('map_salon', {
            center: [53.927321, 27.566664],
            zoom: 17,
            controls: ['smallMapDefaultSet']
        });

        marker_salon = new ymaps.Placemark(map_salon.getCenter(), {
            hintContent: 'г. Минск, ул. Кульман, 31'
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/local/templates/terra/img/content/marker.png',
            iconImageSize: [67, 94],
            iconImageOffset: [-30, -94]
        });

        map_salon.geoObjects.add(marker_salon);
        map_salon.behaviors.disable('scrollZoom');
		
		//---MAP_SKLAD----------
		
		var  map_sklad = new ymaps.Map('map_sklad', {
            center: [53.936030, 27.421515], 
            zoom: 17,
            controls: ['smallMapDefaultSet']
        });

        marker_sklad = new ymaps.Placemark(map_sklad.getCenter(), {
            hintContent: 'пос. Ждановичи, ул. Звездная, 15'
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/local/templates/terra/img/content/marker.png',
            iconImageSize: [67, 94],
            iconImageOffset: [-30, -94]
        });

        map_sklad.geoObjects.add(marker_sklad);
        map_sklad.behaviors.disable('scrollZoom');
    };
	
    function isTouchDevice() {
        return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
    }

    /*-------------
        new JS
    -------------*/
    $(".fancybox-button").fancybox({
        prevEffect      : 'none',
        nextEffect      : 'none',
        margin          : [60, 10, 20, 10],
        closeBtn        : false,
        arrows          : false,
        minHeight       : 5,
        helpers     : {
			media : {},
            title   : { type : 'inside' },
            buttons : {}
        }
    });

    $('#sb-site.pattern').prepend("<div class='pattern_bg pattern_l'><span></span></div><div class='pattern_bg pattern_r'><span></span></div>");
    var patternHeight = $('.pattern_bg').height();
    //console.log(patternHeight);
    var patternNamber = patternHeight / 110;
    var patternBg = Math.floor(patternNamber) * 110;
    //console.log(patternBg);
    $('.pattern_bg span').height(patternBg);

    $('.news, .map-information, .designer-information').prepend("<div class='pattern_in pattern_in_l'><span></span></div><div class='pattern_in pattern_in_r'><span></span></div>");
    var patternHeight = $('.pattern_in').height();
    var patternNamber = patternHeight / 110;
    var patternBg = Math.floor(patternNamber) * 110;
    //console.log(patternBg);
    $('.pattern_in span').height(patternBg);



    var windowHeight = $(window).height(),
        footerH = $('.page-footer').height(),
        heightVisible = i = 0;
        while(windowHeight - $('.page-footer').position().top + footerH > 0){
            if(heightVisible == $('.page-footer').position().top + footerH){
                break;
            }
            showProducts(true);

            heightVisible = $('.page-footer').position().top + footerH;

            if(i++ > 10){
                break;
            }
        }
});


$(window).resize(function () {
    var indexSlider = $('.index__slider__inner');
    var widthBody = $(window).width();
    var indexSlide = $('.index__slider__slide');
    var indexSidebar = $('.index__sidebar');

    var sliderfull = function () {
        indexSlide.css({'background-position': '50%', 'cursor': 'default'});
        indexSidebar.css({'right': -(indexSidebar.width() + 200)}, 1500);
        $('.index__slider__close').css({'opacity': 1, 'display': 'block'}, 2500);
        $('.index__slider__inner').css({'left': 0}, 2500);
    };
    var sliderfull_close = function () {
        indexSlide.css({
            'background-position': '0 0',
            'cursor': 'url(/local/templates/terra/img/content/zoom-cursor.cur), url(/local/templates/terra/img/content/zoom-cursor.cur), url(/local/templates/terra/img/content/zoom-cursor.png), auto'
        });
        indexSidebar.css({'right': 0}, 1500);
        $(this).css({'opacity': 0, 'display': 'none'}, 2500);
        $('.index__slider__inner').css({'left': -25 + '%'}, 2500);
        // indexSlider.slick('slickPlay');
    };


    if ($(window).width() < 961) {
        $('.index__slider__close').css({'opacity': 0, 'display': 'none'});
        $('.index__slider__inner').css({'left': 0});
        indexSlide.css({'cursor': 'default'});

        $('.index__slider__slide').unbind('click');
        $('.index__slider__close').unbind('click');
    } else {
        $('.index__slider__close').css({'opacity': 0, 'display': 'none'});
        $('.index__slider__inner').css({'left': '-25%'});
        indexSidebar.css({'right': 0}, 1500);
        indexSlide.css({'cursor': 'url(/local/templates/terra/img/content/zoom-cursor.cur), url(/local/templates/terra/img/content/zoom-cursor.cur), url(/local/templates/terra/img/content/zoom-cursor.png), auto'});

        $('.index__slider__slide').bind('click', sliderfull);
        $('.index__slider__close').bind('click', sliderfull_close);

    }
});

function showProducts(isNewPage, changeFilter, ischangeprice) {
    var tabId = $('.catalog-tabs li.active a').attr('data-id');
    var $tab = $(tabId);

    var sectionId = $tab.attr('data-section-id');
    var fabricId = $tab.attr('data-fabric-id');

    var colorCount = $("input[name='color-item']:checked").length;

    var totalVisibleElements = 0;
    var initTotalVisibleElements = 0;
    if (isNewPage) {
        //console.log(isNewPage);
        $tab.find('li.catalog-list__item').each(function () {
            if ($(this).css('display') == 'inline-block') {
                initTotalVisibleElements++;
            }
        });
    }

    var arSectionShow = [];
    var arFabricShow = [];
    var arColourShow = [];
    var arPriceShow = [];

    arPriceShow['max'] = 0;
    arPriceShow['min'] = 0;


    $tab.find('li.catalog-list__item').each(function () {
        var showElement = false;


        var currentprice = +$(this).attr('data-price');
        if(changeFilter == true) {
            var priceFlag = false;
            var fabricFlag = false;
            var sectionFlag = false;
            var colorFlag = false;

            if (currentprice >= +filterPriceFrom && currentprice <= +filterPriceTo)  priceFlag = true;
            if ($(this).attr('data-fabric-id') == fabricId || fabricId < 1) fabricFlag = true;
            if ($(this).attr('data-section-id') == sectionId || sectionId < 1) sectionFlag = true;
            if ($(this).attr('data-display') == "true" || colorCount < 1) colorFlag = true;

            if (priceFlag == true && fabricFlag == true && colorFlag == true) {
                arSectionShow[$(this).data('section-id')] = $(this).data('section-id');
            }
            if (priceFlag == true && sectionFlag == true && colorFlag == true) {
                arFabricShow[$(this).data('fabric-id')] = $(this).data('fabric-id');
            }
            if (priceFlag == true && sectionFlag == true && fabricFlag == true) {
                var arOneProd = [];
                var splitcolor = $(this).data('colors') + "|";
                arOneProd = splitcolor.split('|');
                $.each(arOneProd, function (i, item) {
                    if (item.length > 0) {
                        arColourShow[item] = item;
                    }
                });
            }
            if (colorFlag == true && sectionFlag == true && fabricFlag == true ) {
               // if (arPriceShow['min'] < 1) arPriceShow['min'] = $(this).data('price');
                //if (arPriceShow['max'] < 1) arPriceShow['max'] = $(this).data('price');
                if (arPriceShow['min'] < 1) arPriceShow['min'] = currentprice;
                if (arPriceShow['max'] < 1) arPriceShow['max'] = currentprice;

                if (currentprice > arPriceShow['max']) {
                    arPriceShow['max'] = currentprice;
                }
                if (currentprice < arPriceShow['min']) {
                    arPriceShow['min'] = currentprice;
                }
            }
        }

        //if ($(this).attr('data-price') >= filterPriceFrom && $(this).attr('data-price') <= filterPriceTo) {
        if (currentprice >= +filterPriceFrom && currentprice <= +filterPriceTo) {

            if (sectionId > 0 && fabricId > 0) {
                if ($(this).attr('data-section-id') == sectionId && $(this).attr('data-fabric-id') == fabricId) showElement = true;
            } else if (sectionId > 0) {
                if ($(this).attr('data-section-id') == sectionId) showElement = true;
            } else if (fabricId > 0) {
                if ($(this).attr('data-fabric-id') == fabricId) showElement = true;
            } else {
                showElement = true;
            }

            if(colorCount > 0){
                if($(this).attr('data-display') != "true") showElement = false;
            }
        }

        if (showElement) {
            if ((initTotalVisibleElements * 1 + onPage * 1) > totalVisibleElements) {
                totalVisibleElements++;
            } else {
                showElement = false;
            }
        }

        if (showElement) {
            $(this).css('display', 'inline-block');
        } else {
            //$(this).css('display', 'none');
        }
    });

    if(changeFilter == true){
        hiddenParameters(arSectionShow, arFabricShow, arColourShow, arPriceShow, ischangeprice);
    }

}

function setMinMaxPrice() {
    var tabId = $('.catalog-tabs li.active a').attr('data-id');
    var $tab = $(tabId);

    var sectionId = $tab.attr('data-section-id');
    var fabricId = $tab.attr('data-fabric-id');

    $tab.find('li.catalog-list__item').each(function () {
        var showElement = false;

        if (sectionId > 0 && fabricId > 0) {
            if ($(this).attr('data-section-id') == sectionId && $(this).attr('data-fabric-id') == fabricId) showElement = true;
        } else if (sectionId > 0) {
            if ($(this).attr('data-section-id') == sectionId) showElement = true;
        } else if (fabricId > 0) {
            if ($(this).attr('data-fabric-id') == fabricId) showElement = true;
        } else {
            showElement = true;
        }

        if (showElement) {
            var price = parseFloat($(this).attr('data-price'));

            if (price < minPrice) minPrice = price;
            if (price > maxPrice) maxPrice = price;
        }
    });
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";

    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}


function hiddenParameters(section, fabric, color, price, ischangeprice){
    var sectionSet = false;
    var fabricSet = false;
    var colorSet = false;
    var priceSet = false;

    /*
    $(".filter-list__item.section .filter-submenu__item a").css('display', 'none');
    $(".filter-list__item.section a[data-section-id='0']").css('display', 'block');
    section.forEach(function(item, i, arr) {
        $(".filter-list__item.section a[data-section-id='"+i+"']").css('display', 'block');
    });

    $(".filter-list__item.fabric .filter-submenu__item a").css('display', 'none');
    $(".filter-list__item.fabric a[data-fabric-id='0']").css('display', 'block');
    fabric.forEach(function(item, i, arr) {
        $(".filter-list__item.fabric a[data-fabric-id='"+i+"']").css('display', 'block');
    });
    */

    $(".filter-list__item.color .filter-submenu li").css('display', 'none');
    if(Object.keys(color).length <= 0){
        $(".filter-list__item.color > a").css('pointer-events', 'none');
        $(".filter-list__item.color > a").css('opacity', '0.4');
    }else{
        $(".filter-list__item.color > a").css('pointer-events', 'auto');
        $(".filter-list__item.color > a").css('opacity', '1');
    }
    color.forEach(function(item, i, arr) {
        $(".filter-list__item.color li[data-color-id='"+i+"']").css('display', 'block');
    });

    price['max'] = Math.ceil(price['max'] * 100) / 100;
    price['min'] = Math.floor(price['min'] * 100) / 100;

    if($('.catalog-tabs li.active a').length > 0){
       // var tabId = $('.catalog-tabs li.active a').attr('data-id').replace('#catalog-tab', '');
        //$( "#slider-range"+tabId ).slider('values', [price["min"],price["max"]]);

        if(!ischangeprice){
            $("input[name='price_from']").val(price['min']);
            $("input[name='price_to']").val(price['max']);

           // $("#amount" + tabId).val(BX.util.number_format($("#slider-range" + tabId).slider("values", 0), 2, '.', ' ') +
            //    " до " + BX.util.number_format($("#slider-range" + tabId).slider("values", 1), 2, '.', ' '));
        }
    }
}

function changeColor(element){
    var arItem = $("input[name='color-item']:checked");

    if(arItem.length >= 1){

        $(".catalog-list__item").attr('data-display', 'false');
        $.each(arItem, function(i, item) {

            $(".catalog-list__item."+$(item).val()).attr('data-display', 'true');
            $("."+$(item).val()).removeClass('color-hidden');
        });

    }else{
        $(".catalog-list__item").attr('data-display', 'true');
        $(".color-hidden").removeClass('color-hidden');
    }

    showProducts(false, true);

}



/*--------------------------------------------------------------------------------*/

$(document).mouseup(function(e){

    var container8 = $(".filter-list__item");
        if (!container8.is(e.target) && container8.has(e.target).length === 0){
        $('.filter-list__item').find('a').removeClass('visible');

        $('.filter-submenu').fadeOut();
    };
});


/*-------- new JS -----------*/

$(document).mouseup(function(e){
    var container = $(".search_form");
        if (!container.is(e.target) && container.has(e.target).length === 0){
        $('.search_form-cont').fadeOut();
    };
})

    /*-------- new JS -----------*/

$(document).ready(function() {

    if($('.catalog.search').length > 0){
        $(".catalog-tab_content:not(.cancel)").hide();
        $("ul.catalog-tabs:not(.cancel) li:first").addClass("actives").show();
        $(".catalog-tab_content:not(.cancel):first").show();

        $("ul.catalog-tabs:not(.cancel) li").click(function() {
            $("ul.catalog-tabs li").removeClass("actives");
            $(this).addClass("actives");
            $(".catalog-tab_content").hide();
            var activeTab = $(this).find("a").attr("href");
            $(activeTab).fadeIn();
            return false;
        });
    }


    $('.search_form--lnk').on('click', function(){
        $('.search_form-cont').show();

        return false;
    });



    if ($('.you_watched_sl').length>1) {
        $('.you_watched_sl').slick({

            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        })
    };

    $('.about-lnk-show').on('click', function () {
        $('.about-hide').slideDown();
        $(this).hide();
        $('.about-lnk-hide').show();
        return false;
    });
    $('.about-lnk-hide').on('click', function () {
        $('.about-hide').slideUp();
        $(this).hide();
        $('.about-lnk-show').show();
        return false;
    });







//});
/*поиск*/
    $(document).click(function (e){
        var div = $(".search_form-cont");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            $('.search_form-cont').hide(200);
        }
    });

    $('.menu_tabs-mob').click(function(){
		if($(this).hasClass('active')) {
			$('.menu_tabs .catalog-tabs').slideUp();
			$(this).removeClass('active');
		} else {
			$('.menu_tabs .catalog-tabs').slideDown();
			$(this).addClass('active');
		};
		return false;
	});

	$("ul.catalog-tabs.cancel li").click(function() {
		if($('.menu_tabs-mob').hasClass('active')) {
			var actMenu = $(this).text();
			$('.menu_tabs-mob').text(actMenu);
			$('.menu_tabs-mob').removeClass('active');
			$('.menu_tabs .catalog-tabs').slideUp()
		}
	});
    if($("ul.catalog-tabs.cancel li.active").length > 0){
        var actMenu = $("ul.catalog-tabs.cancel li.active").text();
        $('.menu_tabs-mob').text(actMenu);
    }

	$('.search_form--lnk').click(function(){
		$('.search_form-cont').fadeIn();
		$('.search_form-cont input').focus();
		return false;
	});

	if ($('.you_watched_sl').length>0) {
		$('.you_watched_sl').slick({

		    slidesToShow: 5,
  			slidesToScroll: 5,
		    responsive: [
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
  						slidesToScroll: 2
					}
				}
			]
		})
	};

	if($('.search_page').length) {
		if($('.catalog').length) {} else {
			var minHeight = $(window).height() - 378;
			$('.search_page').css({'min-height' : minHeight})
		}
	};


    //$('[name = "s"]').hide();

	//filter-fixed
	if($(window).width() < 761) {
        if($(this).scrollTop() > 410) {
            $(".filter_open").addClass("filter_open-fixed");
        }
        else {
            $(".filter_open").removeClass("filter_open-fixed");
        };
        $(window).scroll(function() {
            if($(this).scrollTop() > 410) {
                $(".filter_open").addClass("filter_open-fixed");
            }
            else {
                $(".filter_open").removeClass("filter_open-fixed");
            };
        });
    };
    $(window).resize(function() {
        if($(window).width() < 761) {
            if($(this).scrollTop() > 410) {
                $(".filter_open").addClass("filter_open-fixed");
            }
            else {
                $(".filter_open").removeClass("filter_open-fixed");
            };
            $(window).scroll(function() {
                if($(this).scrollTop() > 410) {
                    $(".filter_open").addClass("filter_open-fixed");
                }
                else {
                    $(".filter_open").removeClass("filter_open-fixed");
                };
            });
        };
    });
});

//Фикс кнопок


 var formWrapper = $(".calculation__form-wrapper"),
	 formFixed = $(".calculation__field--fixed");

    function getFixed() {
        var form = $(".calculation__form"),
            formHeight = form.height(),
            windowHeight = $(window).height(),
            pageFooter = $(".page-footer"),
            fixedElem = $(".calculation__field--fixed");


        if (formHeight > windowHeight && form.hasClass("active")) {
			fixedElem.addClass("fixed");
        } else {
			fixedElem.removeClass("fixed");
        }


   };

    function getScroll() {
       var form = $(".calculation__form"),
           formHeight = form.height(),
           fixedElem = $(".calculation__field--fixed");

       	$(window).scroll(function(){
			if($(this).scrollTop() > formHeight/5) {
              fixedElem.removeClass("fixed");
			} else if ($(this).scrollTop() == 0 && $(window).width < 768) {
				fixedElem.addClass("fixed");
			};
       });

    };



	function getWidth(){
       var windowWidth = $(window).width(),
           fixedElem = $(".calculation__field--fixed");

		if(windowWidth > 768){
		  $(".calculation__trigger").show();
          $(".calculation__trigger").on("click", function(e){
       			e.preventDefault();
				
				if(!$(".calculation__form").hasClass("active")){
         			$(".calculation__form").addClass("active");
		 			fixedElem.show();
					setTimeout(function(){getFixed();},300);
					$('body,html').animate({scrollTop:0},800);
			    } else {
            	   $(".calculation__form").removeClass("active");
            	   fixedElem.hide();
        	    }
    		});
		} else {
		  $(".calculation__trigger").off("click",function(){});
          $(".calculation__trigger").hide();
      };
    };

	/*$("body").click(function(e) {
		var form = $(".calculation__form"),
			fixedElem = $(".calculation__field--fixed");

	    	form.removeClass("active");
			fixedElem.removeClass("fixed");
	});*/



    $(".calculation__form-close").on("click", function(e){
		var fixedElem = $(".calculation__field--fixed");
    	e.preventDefault();
    	$(".calculation__form").removeClass("active");
        if($('.calculation__form').hasClass('calculation__form_for_page')==false) {
            fixedElem.hide();
        }
		 fixedElem.removeClass("fixed");
    });


	$(".mobile-calc-icon").on("click", function(e){
		var fixedElem = $(".calculation__field--fixed");
		e.preventDefault();
    	if(!$(".calculation__form").hasClass("active")){
         $(".calculation__form").addClass("active");
         fixedElem.show();
            setTimeout(function(){getFixed();},300);

          $('body,html').animate({scrollTop:0},800);
		} else {
            $(".calculation__form").removeClass("active");
            fixedElem.hide();

        }
    });

     $(window).on('load', getScroll);
     $(window).on('load resize', getFixed);
     $(window).on('load resize', getWidth);