$(document).ready(function() {			
	var fmb = $('.fixed-message-box');	
	var ff = $('.fixed-footer');	
	var fmbbbr = $('.fixed-message-box__btn-block.radio');	
	var fmbtmr = $('.fixed-message-box__time-message.radio.1');	
	var fmbtmr2 = $('.fixed-message-box__time-message.radio.2');	
	var ffsb = $('.fixed-footer__send-btn');	
	var fmbrg = $('.fixed-message-box__radio-group.1');
	var fmbrg2 = $('.fixed-message-box__radio-group.2');	
	var fmbbb = $('.fixed-message-box__btn-block');	
	var fi = $('.footer_input');
	var quest1 = "";
	var quest2 = "";	
	var quest3 = "";	
	$.ajax({
		url: '/setcookies.php',
		type: 'POST',		
		data: { 'CHECK_COOKIE': 'Y'},
		success: function(data) {
			console.log(data);
			if(data == "ERROR") {
				
			} else if(data == "CHECK") {
				if($(window).width() > 768) {
					$(".fixed-window").delay(1000).slideDown(600);
				}
				$.ajax({			
					url: "/setcookies.php",			
					type: 'POST',				
					data: {	'CHECK_STEP' : 'Y'},			
					success: function(data) {
						jump(data);
					}			
				});	
			} else if(data == "OK") {
				var res = {
					'SESSION' : $('.bitrix_sessid_dialog').text(),
				};		
				var interv = setInterval(function() {
					$.ajax({
						url: "/checkusersession.php",
						type: 'POST',
						data: res,				
						success: function(data) {
							if(data == "OK") {
								if($(window).width() > 768){
									$(".fixed-window").delay(2000).slideDown(600);
								}
										
								$.ajax({			
									url: "/setcookies.php",			
									type: 'POST',				
									data: {	'CHECK_COOKIE_NEXT' : 'Y'},			
									success: function(data) {}			
								});	
								clearInterval(interv);
							}
						},
					});
				}, 1000);
			}				
		},
	});
	
	
	
	const node = document.getElementById('footer_input');
	
	node.addEventListener('keydown', function onEvent(event) {
		if (event.key === "Enter") {			
			ffsb.click();
			return false;
		}
	});
	
	$('input[type=radio][name=tmp]').change(function() {        
		if (this.value == 'Другое') {			
			ff.css('display', 'block');				
			fmbbbr.css('display', 'none');			        
		} else if (this.value != 'Другое') {            
			ff.css('display', 'none');			
			fmbbbr.css('display', 'block');		        
		}    
	});	
	$('input[type=radio][name=tmp1]').change(function() {
		ff.css('display', 'none');			
		fmbbbr.css('display', 'block');
	});			
	$('.submit_yes').on('click', function() {				
		if($(this).hasClass('1')) {			
			fmb.find('p:first-child').text('Мы попросим Вас ответить на три вопроса, после чего предоставим специальный код для получения скидки. Ваши ответы помогут нам стать лучше и удобнее для вас.');			
			$(this).text("Да, согласен");		
			$(this).removeClass("1").addClass('2');	
			$.ajax({			
				url: "/setcookies.php",			
				type: 'POST',				
				data: {	'SET_STEP' : 1},			
				success: function(data) {}			
			});				
		} else if($(this).hasClass('2')) {
			fmb.find('p:first-child').text('Где вы услышали/узнали о нашем салоне?');	
			fmbtmr2.css("display", 'block');						
			fmbbb.css("display", 'none');		
			fmbrg2.css("display", 'block');			
			$(this).removeClass("2").addClass('3');	
			$.ajax({			
				url: "/setcookies.php",			
				type: 'POST',				
				data: {	'SET_STEP' : 2},			
				success: function(data) {}			
			});	
		} else if($(this).hasClass('3')) {
			$.ajax({			
				url: "/setcookies.php",			
				type: 'POST',				
				data: {	'SET_STEP' : 4},			
				success: function(data) {}			
			});				
			fmb.find('p:first-child').text('Какие салоны, магазины керамической плитки вы еще рассматриваете для покупки плитки/сантехники?');					
			ff.css('display', 'block');			
			fmbbb.css("display","none");			
			ffsb.addClass('3');					
		}	
	});		
	$('.fixed-footer__send-btn').on('click', function() {				
		if($(this).hasClass('3')) {				
			quest1 = fi.val();
			$.ajax({			
				url: "/setcookies.php",			
				type: 'POST',				
				data: {	'SET_STEP' : 5, 'quest':true, 'quest1': quest1},			
				success: function(data) {}			
			});			
			fi.val("");			
			ff.css('display', 'none');		
			fmb.find('p:first-child').text('Какой главный фактор определит покупку плитки/сантехники в том или ином месте?');			
			fmbrg.css('display', 'block');			
			fmbtmr.css("display", "block");			
			$(this).removeClass("3").addClass('4');		
		} else if($(this).hasClass('4')) {			
			if(fi.val() == "") {
				if($('input[type=radio][name=tmp]:checked').val() == "Цена") {
					quest2 = "";
				} else {
					quest2 = $('input[type=radio][name=tmp]:checked').val();
				}
			} else {
				quest2 = fi.val();
			}
			$.ajax({			
				url: "/setcookies.php",			
				type: 'POST',				
				data: {	'SET_STEP' : 6, 'quest':true, 'quest2': quest2},			
				success: function(data) {}			
			});				
			fi.val("");			
			fmbbbr.css("display","none");				
			fmbrg.css('display', 'none');			
			fmbtmr.css("display","none");			
			$(this).removeClass("4").addClass('5');			
			fmb.find('p:first-child').text('Спасибо за Ваши ответы! Напишите, пожалуйста, ваши имя и фамилию, чтобы мы предоставили вам специальный код для получения скидки -5% в нашем салоне?');
		} else if($(this).hasClass('5')) {
			$.ajax({			
				url: "/setcookies.php",			
				type: 'POST',				
				data: {	'SET_STEP' : 7},			
				success: function(data) {}			
			});				
			if(fi.val() == "") {								
				$('.p.error').css("display","block").text('Вы не ввели данные.');					
				$('.fixed-message-box__time-message.error').css("display","block");					
				fmb.append();				
				return;							
			}			
			$('.p.error').css("display","none");				
			$('.fixed-message-box__time-message.error').css("display","none");							
			ff.css('display', 'none');						
			fmb.find('p:first-child').text('Это ваш код на получение скидки в размере -5% в салоне Terra Cotta. При покупке, сообщите его нашим консультантам. Помните,  что скидка не суммируется с другими акциями в салоне и не  распространяется на мозаику производства Китай и коллекции польской  фабрики Opozchno. Воспользоваться кодом можно лишь единожды. Срок действия кода - 2 месяца. Ждем Вас в нашем салоне на Кульман, 31');
			var code = randomInteger(1000, 9999);
			$.ajax({			
				url: "/setcookies.php",			
				type: 'POST',	
				dataType: 'json',				
				data: {'GET_QUESTS' : 'Y'},			
				success: function(data) {
					console.log(data);
					var res = {				
						'CODE' : code,				
						'FIO' : fi.val(),
						'QUEST1' : data['quest1'],
						'QUEST2' : data['quest2'],
						'QUEST3' : data['quest3'],
					};									
					$.ajax({				
						url: "/adddiscountforuser.php",				
						type: 'POST',				
						data: res,								
						success: function(data) {					
							$('.p.code').css('display','block').text(code);					
							$('.fixed-message-box__time-message.code').css('display','block');									
						},			
					});	
					setDialogCookie();		
				}			
			});	
		}	
	});	
	
	function randomInteger(min, max) {		
		var rand = min + Math.random() * (max + 1 - min);		
		rand = Math.floor(rand);		
		return rand;	
	}  	
	
	$('.submit_next').on('click', function() {
		if($(this).hasClass('2')) {		
			quest2 = $('input[type=radio][name=tmp]:checked').val();	
			$.ajax({			
				url: "/setcookies.php",			
				type: 'POST',				
				data: {	'SET_STEP' : 6, 'quest':true, 'quest2': quest2},			
				success: function(data) {}			
			});					
			fmbbbr.css("display","none");				
			fmbrg.css('display', 'none');		
			fmbtmr.css("display","none");		
			ffsb.removeClass("4").addClass('5');		
			fmb.find('p:first-child').text('Спасибо за Ваши ответы! Напишите, пожалуйста, ваши имя и фамилию, чтобы мы предоставили вам специальный код для получения скидки -5% в нашем салоне?');
			ff.css('display', 'block');	
		} else if($(this).hasClass('1')) {			
			quest3 = $('input[type=radio][name=tmp1]:checked').val();
			$.ajax({			
				url: "/setcookies.php",			
				type: 'POST',				
				data: {	'SET_STEP' : 3, 'quest':true, 'quest3': quest3},			
				success: function(data) {}			
			});				
			fmb.find('p:first-child').text('Какие салоны, магазины керамической плитки вы еще рассматриваете для покупки плитки/сантехники?');					
			ff.css('display', 'block');	
			fmbtmr2.css("display", 'none');
			fmbbb.css("display", 'none');
			fmbrg2.css("display", 'none');	
			ffsb.addClass('3');	
			$(this).removeClass('1').addClass('2');					
		}		
	});				
	$(".fixed-header__close-cross").click(function() { 		
		$(".fixed-window").slideUp(600);		
		setDialogCookie();		
	});	
	$(window).resize(function() {		
		if($(window).width() <= 768) {			
			$(".fixed-window").slideUp(600);		
		}	
	});		
	$('.submit_no').on('click', function() {		
		$(".fixed-header__close-cross").click();		
		setDialogCookie();	
	});	

	
	function setDialogCookie() {
		var res = {			
			'SET_COOKIE' : 'Y',		
		};			
		$.ajax({			
			url: "/setcookies.php",			
			type: 'POST',				
			data: res,			
			success: function(data) {}			
		});		
	}

	function jump(count) {		
		if(count >= 1)
			$('.submit_yes').click();
		if(count >= 2)
			$('.submit_yes').click();
		if(count >= 3)
			$('.submit_next').click();
		if(count >= 4)
			$('.submit_yes').click();
		if(count >= 5)
			$('.fixed-footer__send-btn').click();
		if(count >= 6)
			$('.submit_next').click();
		if(count >= 7)
			$('.fixed-footer__send-btn').click();
	}
	
});